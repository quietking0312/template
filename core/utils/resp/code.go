package resp

const (
	Success   = 0   // 成功
	ErrArgs   = 300 // 参数错误
	ErrServer = 500 // 服务错误
)

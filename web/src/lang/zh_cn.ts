export default {
    route: {
        login: '登录',
        dashboard: "首页"
    },
    tagsView: {
        refresh: "刷新",
        close: "关闭",
        closeOther: "关闭其他",
        closeAll: "关闭全部"
    },
    userInfo: {
        btnLogout: "退出登录"
    },
    login: {
        titleLogin: "登录",
        btnLogin: "登录",
        placeholderUsername:"请输入账号",
        placeholderPassword: "请输入密码",
        tipsVersion: "版本",
    }
}
